import os
import sys
import signal
from subprocess import Popen, PIPE, STDOUT
import time
from conf import webots_path, world_path
import argparse


proc = Popen(
    [webots_path, world_path],
    preexec_fn=os.setsid,
)

