# EDIT THIS FILE

# InvaderBot is a simple helper class which has methods to control the robot.
# It offers an interface over Webots' default motor controller.
# You can use it, modify it or you can also use Webots' own commands.
# Using the helper class is recommended because it will later help you to
# transfer your code into a physical robot, which is running on Raspberry Pi.

from invader_bot import InvaderBot

bot = InvaderBot()
bot.setup()

# main loop
steps = 0
while bot.step():

    if steps < 10:
        pass
    elif steps < 14:
        bot.set_motors(-0.75, 0.75)
    elif steps < 110:
        bot.set_motors(1, 1)
    elif steps < 130:
        bot.set_motors(0.75, -0.75)
    elif steps < 170:
        bot.set_motors(1, 1)
    elif steps < 179:
        bot.set_motors(-0.75, 0.75)
    elif steps < 245:
        bot.set_motors(1, 1)
    else:
        bot.set_motors(0, 0)

    steps += 1