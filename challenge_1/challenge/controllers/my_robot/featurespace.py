import numpy as np

def features(bot,fspace):
    balls = bot.get_balls()
    eballs = np.arange(fspace - 3)
    eballs.fill(-100)
    for ball in range(len(balls)):
        for i in range(2):
            eballs[ball * 2 + i] = balls[ball][i]
    botdat = np.array(bot.get_position())
    res = np.concatenate((eballs,botdat),axis=None)
    return res
