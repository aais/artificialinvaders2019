
def rewardf(bot):
    balls = bot.get_balls()
    reward = 0
    for ball in balls:
        reward += sum(ball)
    return - reward
