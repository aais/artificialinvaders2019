import numpy as np
import os
import random
import math

target_x = 0
target_y = 0

x_in = [-1.5, 1.5]
y_in = [-1.5, 1.5]
old_reward = 0
hit_threshold = 0.3
old_pos = [0, 0]
start_reward = 0
pos_set = False

def get_rand_coordinates():
    global target_x
    global target_y
    target_x = float(random.randint(0, 100))* (x_in[1] - x_in[0])/100 + x_in[0]
    target_y = float(random.randint(0, 100))* (y_in[1] - y_in[0])/100 + y_in[0]
    print('new coordinates ', target_x, target_y)

def features(bot,fspace=None):
    pos = bot.get_position()
    pos += [-target_x, -target_y]
    pos[3] += pos[0]
    pos[4] += pos[1]
    return np.array(pos)

def distance(a, b):
    return (a[0] - b[0])**2 + (a[1] - b[1])**2

counter = 0

def rewardf(bot):
    global old_reward
    global old_pos
    global counter
    global pos_set
    global start_reward

    if not pos_set:
        get_rand_coordinates()
    pos = bot.get_position()
    a = pos[0] - target_x
    b = pos[1] - target_y
    dist = math.sqrt(a**2 + b**2)
    counter += 1
    newreward = -dist
    c = math.cos(pos[2]*math.pi/180)
    d = math.sin(pos[2]*math.pi/180)
    newreward -= a*c + d*b
    if not pos_set:
        start_reward = -newreward
        pos_set = True
        old_pos = pos[0:2]
    newreward += start_reward
    reward = newreward + old_reward
    if dist < hit_threshold:
        old_reward += newreward
        print('Hit the target')
        pos_set = False
        #with open('log.heh', 'a+') as fil:
        #    fil.write("Hit" + '\n')
    dist_moved = distance(old_pos, pos)
    old_reward += dist_moved/30
    old_reward -= 0.002
    old_pos = pos[0:2]
    if counter % 20 == 0:
        print(reward)
    return reward

