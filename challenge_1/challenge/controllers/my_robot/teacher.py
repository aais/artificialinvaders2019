import os
import sys
import signal
from subprocess import Popen, PIPE, STDOUT
import time
from conf import webots_path, world_path
import argparse

parser = argparse.ArgumentParser(description="Trainer")
parser.add_argument("-t", "--time", default=20, type=int, help="Time per epoch")
parser.add_argument(
    "-r",
    "--exploration-rate",
    default=1,
    type=float,
    help="Exploration rate in the beginning.",
)
parser.add_argument(
    "-d", "--decay", default=0.99, type=float, help="Exploration decay factor"
)

args = parser.parse_args()

seconds_per_epoch = args.time
exploration_rate = args.exploration_rate
exploration_decay = args.decay

import threading

class Killer:
    def get_rid(self):
        os.killpg(os.getpgid(self.proc.pid), signal.SIGINT)
        t = threading.Thread(target=self.kill)
        t.start()
    def kill(self):
        time.sleep(2.0)
        try:
            pid = os.getpgid(self.proc.pid)
            print("Killing!")
            os.killpg(pid, signal.SIGKILL)
        except Exception as e:
            print(e)
    def __init__(self, proc):
        self.proc = proc
        self.get_rid()


def update_teaching_file():
    global exploration_rate
    with open("teaching_state.py", "w+") as fn:
        fn.write("expl = " + str(exploration_rate))
        exploration_rate *= exploration_decay


proc = None
for _ in range(1000):
    try:
        print("New epoch, new kujeet")
        update_teaching_file()
        proc = Popen(
            [webots_path, "--batch", "--mode=fast", "--stderr", "--stdout", world_path],
            stdout=None,
            stdin=PIPE,
            stderr=STDOUT,
            preexec_fn=os.setsid,
        )
        time.sleep(seconds_per_epoch)
    finally:
        Killer(proc)
