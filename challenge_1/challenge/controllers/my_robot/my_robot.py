import numpy as np
import os
from invader_bot import InvaderBot
from model import DQNSolver
from locationgen import rewardf
from locationgen import features
from teaching_state import expl
import random


model_name = "./locmodel.h5"
bot = InvaderBot()
bot.setup()
training = True
fspace = 5
brain = DQNSolver(fspace, 4, expl)
last_state = np.arange(fspace)
last_state.fill(-100)
last_state = last_state.reshape(1, fspace)

if os.path.exists(model_name):
    brain.load(model_name)  # load old model if exists
    print("Found old model")


l = 0
r = 0
def set_motorsh(bot, l, r):
    ran = (random.random()-0.5)/10 + 1
    ran2 = (random.random()-0.5)/10 + 1
    l = l*ran
    r = r*ran2
    l = min(l, 1)
    r = min(r, 1)
    bot.set_motors(l, r)


def do_something(bot, num):
    global l
    global r
    stp=0.8
    if num == 0:
            l = -stp
            r = -stp
    elif num == 1:
            r = stp
            l = stp
    elif num == 2:
        r = 0.1
        l = -0.1
    elif num == 3:
        l = 0.1
        r = -0.1
    set_motorsh(bot, l, r)

print("start")
try:
    while bot.step():
        # get current time
        time = bot.get_time()
        state = features(bot, fspace).reshape(1, -1)
        action = brain.act(state, training)
        do_something(bot, action)
        brain.remember(last_state, action, rewardf(bot), state, False)
        last_state = state
        brain.experience_replay()
        for i in range(20):
            if not bot.step():
                break
finally:
    print('saving')
    brain.save(model_name)

